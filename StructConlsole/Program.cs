﻿using System;
using System.Text;

namespace WorkersApp
{

    struct Company
    {

        public string Name;
        public string Position;
        public double Salary;

        public Company(string name, string position, double salary)
        {
            Name = name;
            Position = position;
            Salary = salary;
        }
    }

    struct Worker
    {
        public string Name;
        public int Year;
        public int Month;
        public Company WorkPlace;

        public Worker(string name, int year, int month, Company workPlace)
        {
            Name = name;
            Year = year;
            Month = month;
            WorkPlace = workPlace;
        }

        public int GetWorkExperience()
        {
            DateTime now = DateTime.Now;
            int totalMonths = (now.Year - Year) * 12 + (now.Month - Month);
            return totalMonths;
        }

        public double GetTotalMoney()
        {
            int totalMonths = GetWorkExperience();
            return totalMonths * WorkPlace.Salary;

            static void PrintWorker(Worker worker)
            {
                Console.WriteLine($"Ім'я: {worker.Name}");
                Console.WriteLine($"Рік початку роботи: {worker.Year}");
                Console.WriteLine($"Місяць початку роботи: {worker.Month}");
                Console.WriteLine($"Назва компанії: {worker.WorkPlace.Name}");
                Console.WriteLine($"Посада: {worker.WorkPlace.Position}");
                Console.WriteLine($"Заробітна плата: {worker.WorkPlace.Salary}");
                Console.WriteLine($"Загально грошей: {worker.GetTotalMoney()}");
            }
        }
    }

    class Program
    {
        static Worker[] ReadWorkersArray()
        {

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №5";
            Console.SetWindowSize(100, 25);
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Clear();


            Console.Write("Введіть кількість робітників: ");
            int n = int.Parse(Console.ReadLine());

            Worker[] workers = new Worker[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Введіть інформацію про працівника #{i + 1}");
                Console.Write("Ім'я: ");
                string name = Console.ReadLine();
                Console.Write("Рік початку роботи: ");
                int year = int.Parse(Console.ReadLine());
                Console.Write("Місяць початку роботи: ");
                int month = int.Parse(Console.ReadLine());
                Console.Write("Назва компанії: ");
                string companyName = Console.ReadLine();
                Console.Write("Посада: ");
                string position = Console.ReadLine();
                Console.Write("Заробітна плата: ");
                double salary = double.Parse(Console.ReadLine());

                Company company = new Company(companyName, position, salary);
                Worker worker = new Worker(name, year, month, company);
                workers[i] = worker;
                Console.Clear();
            }

            return workers;
        }

        static void PrintWorker(Worker worker)
        {
           
            Console.WriteLine($"Ім'я: {worker.Name}");
            Console.WriteLine($"Рік початку роботи: {worker.Year}");
            Console.WriteLine($"Місяць початку роботи: {worker.Month}");
            Console.WriteLine($"Назва компанії: {worker.WorkPlace.Name}");
            Console.WriteLine($"Посада: {worker.WorkPlace.Position}");
            Console.WriteLine($"Заробітна плата: {worker.WorkPlace.Salary}");

        }

        static void PrintWorkers(Worker[] workers)
        {
            foreach (Worker worker in workers)
            {
                PrintWorker(worker);
                Console.WriteLine();
            }
        }

        static void GetWorkersInfo(Worker[] workers, out double minSalary, out double maxSalary)
        {
            minSalary = double.MaxValue;
            maxSalary = double.MinValue;

            foreach (Worker worker in workers)
            {
                double salary = worker.WorkPlace.Salary;
                if (salary < minSalary)
                {
                    minSalary = salary;
                }
                if (salary > maxSalary)
                {
                    maxSalary = salary;
                }
            }
        }

        static void SortWorkerBySalary(Worker[] workers)
        {
            Array.Sort(workers, (w1, w2) => w2.WorkPlace.Salary.CompareTo(w1.WorkPlace.Salary));
        }

        static void SortWorkerByWorkExperience(Worker[] workers)
        {
            Array.Sort(workers, (w1, w2) => w1.GetWorkExperience().CompareTo(w2.GetWorkExperience()));
        }

        static void Main(string[] args)
        {
            Worker[] workers = ReadWorkersArray();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Усі робітники:");
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkBlue;

            PrintWorkers(workers);

            double minSalary, maxSalary;
            GetWorkersInfo(workers, out minSalary, out maxSalary);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Мінімальна зарплата: {minSalary}, Максимальна зарплата: {maxSalary}");
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkBlue;

            SortWorkerBySalary(workers);
            Console.WriteLine("Робітники за зарплатою:");
            PrintWorkers(workers);

        }




    }
}
